package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 * 
 * Figura 5.1: WhileCounter.java
 * Repeti��o controlada por contador com a instru��o de repeti��o while.
 */
public class WhileCounter {

	public static void main(String[] args) {
		int counter = 0; // declara e inicializa a vari�vel de controle

		while (++counter <= 10) { // condi��o de continua��o do loop
			System.out.printf("%d ", counter);
			// ++counter; // vari�vel de controle de incremento
		}
	}
	
} // fim da classe WhileCounter
