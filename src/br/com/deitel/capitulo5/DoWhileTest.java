package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 * 
 * Figura 5.7: DoWhileTest.java
 * instru��o de repeti��o do...while.
 */
public class DoWhileTest {

	public static void main(String[] args) {
		int counter = 1;

		// executa pelo menos uma vez
		do {
			System.out.printf("%d ", counter);
			counter++;
		} while (counter <= 10);

		System.out.println();
	}

} // fim da classe DoWhileTest
