package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 * 
 * Figura 5.12: AutoPolicyTest.java 
 * Demonstrando Strings em um switch.
 */
public class AutoPolicyTest {

	/* Voc� foi contratado por uma companhia de seguros de autom�vel que atende
	  estes estados do nordeste dos Estados Unidos � Connecticut, Maine,
	  Massachusetts, New Hampshire, Nova Jersey, Nova York, Pensilv�nia, Rhode
	  Island e Vermont. A empresa quer que voc� crie um programa que produz um
	  relat�rio indicando para cada uma das ap�lices de seguro de autom�vel se a
	  ap�lice � v�lida em um estado com seguro de autom�vel �sem culpa� (modalidade
	  de seguro em que o segurado � indenizado independentemente de sua
	  responsabilidade no sinistro) � Massachusetts, Nova Jersey, Nova York e
	  Pensilv�nia. */
	public static void main(String[] args) {
		AutoPolicy autoPolicy = new AutoPolicy(1, "Toyota Camry", "NJ");
		AutoPolicy autoPolicy2 = new AutoPolicy(2, "Ford Fusion", "ME");

		policyInNoFaultState(autoPolicy);
		policyInNoFaultState(autoPolicy2);
	}

	// m�todo que mostra se um AutoPolicy est� em um estado com seguro de autom�vel �sem culpa�
	public static void policyInNoFaultState(AutoPolicy policy) {
		System.out.println("The auto policy:");
		System.out.printf("Account #: %d; Car: %s; State %s %s a no-fault state%n%n", 
				policy.getAccountNumber(),
				policy.getMakeAndModel(), 
				policy.getState(), 
				(policy.isNoFaultState() ? "is" : "is not"));
	}

} // fim da classe AutoPolicyTest
