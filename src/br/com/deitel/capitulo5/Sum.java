package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 * 
 * Figura 5.5: Sum.java 
 * Somando inteiros com a instru��o for.
 */
public class Sum {

	public static void main(String[] args) {
		int total = 0;

		System.out.printf("Numbers ");
		// total de inteiros pares de 2 a 20
		for (int number = 2; number <= 20; number += 2) {
			total += number;
			System.out.printf("%d ", number);
		}

		System.out.printf("%nSum is %d%n", total);
	}

} // fim da classe Sum
