package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 * 
 * Figura 5.2: ForCounter.java
 * Repetição controlada por contador com a instrução de repetição for.
 */
public class ForCounter {

	public static void main(String[] args) {
		
		// o cabeçalho da instrução for inclui inicialização, condição de continuação do loop e incremento
		for (int counter = 0; counter <= 10; counter++) {
			System.out.printf("%d ", counter);
		}

		System.out.println();
	}
	
} // fim da classe ForCounter