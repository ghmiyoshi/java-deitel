package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 *
 * Figura 5.11: AutoPolicy.java
 * Classe que representa uma ap�lice de seguro de autom�vel.
 */
public class AutoPolicy {

	private int accountNumber; // armazena o n�mero da conta da ap�lice
	private String makeAndModel; // armazena a marca e o modelo do carro
	private String state; // armazenar a sigla do estado de dois caracteres

	public AutoPolicy(int accountNumber, String makeAndModel, String state) {
		this.accountNumber = accountNumber;
		this.makeAndModel = makeAndModel;
		this.state = state;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getMakeAndModel() {
		return makeAndModel;
	}

	public void setMakeAndModel(String makeAndModel) {
		this.makeAndModel = makeAndModel;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isNoFaultState() {
		boolean noFaultState;

		switch (getState()) {
		case "MA":
		case "NJ":
		case "NY":
		case "PA":
			noFaultState = true;
			break;
		default:
			noFaultState = false;
			break;
		}

		return noFaultState;
	}

} // fim da classe AutoPolicy