package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 * 
 * Figura 5.14: ContinueTest.java
 * Instru��o continue que termina uma itera��o de uma instru��o for.
 * 
 */
public class ContinueTest {

	public static void main(String[] args) {
		int notUsed = 0;

		for (int count = 1; count <= 10; count++) { // faz o loop 10 vezes
			if (count == 6) {
				notUsed = count;
				continue; // pula o c�digo restante no corpo do loop se a contagem for 5
			}

			System.out.printf("%d ", count);
		}

		System.out.printf("%nUsed continue to skip printing %d%n", notUsed);
	}

} // fim da classe ContinueTest
