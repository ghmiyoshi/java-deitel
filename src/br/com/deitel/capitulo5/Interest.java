package br.com.deitel.capitulo5;

/**
 * @author Gabriel Hideki
 *
 * Figura 5.6: Interest.java
 * C�lculos de juros compostos com for.
 */
public class Interest {

	public static void main(String[] args) {
		/* Uma pessoa investe US$ 1.000 em uma conta-poupan�a que rende juros de 5% ao ano. 
		 Supondo que todo o juro seja aplicado, calcule e imprima a quantia de dinheiro na 
		 conta no fim de cada ano por 10 anos. */
		double amount; // quantia em dep�sito ao fim de cada ano
		double rate = 0.05; // taxa de juros
		double principal = 1000.0; // quantidade inicial antes dos juros

		// exibe cabe�alhos
		System.out.printf("%s%20s %n", "Year", "Amount on deposit");

		// calcula quantidade de dep�sito para cada um dos dez anos
		for (int year = 1; year <= 10; year++) {
			amount = principal * Math.pow(1.0 + rate, year);

			// exibe o ano e a quantidade
			System.out.printf("%4d%20.2f%n", year, amount);
		}
	}

} // fim da classe Interest
