package br.com.deitel.capitulo14;

/**
 * @author Gabriel Hideki
 * 
 * Figura 14.5: StringIndexMethods.java
 * M�todos de pesquisa de String indexOf e lastIndexOf.
 */
public class StringIndexMethods {
	
	public static void main(String[] args) {
		String letters = "abcdefghijklmabcdefghijklm";

		/* testa indexOf para localizar um caractere em uma string
		o m�todo indexOf para localizar a primeira ocorr�ncia de um caractere em uma String. Se o m�todo
		String localizar o caractere, ele retorna o �ndice do caractere na String � caso contr�rio, retorna �1. H� duas vers�es de indexOf
		que procuram caracteres em uma String. A express�o na linha 12 utiliza a vers�o do m�todo indexOf que aceita uma representa��o
		de inteiro do caractere a localizar. A express�o na linha 14 utiliza outra vers�o do m�todo indexOf, que aceita dois argumentos
		inteiros � o caractere e o �ndice inicial em que a pesquisa da String deve iniciar. */
		System.out.printf("'c' is located at index %d%n", letters.indexOf('c'));
		System.out.printf("'a' is located at index %d%n", letters.indexOf('a', 1)); 
		System.out.printf("'a' is located at index %d%n", letters.indexOf('a')); 
		System.out.printf("'$' is located at index %d%n%n", letters.indexOf('$'));

		/* testa lastIndexOf para localizar um caractere em uma string
		o m�todo lastIndexOf para localizar a �ltima ocorr�ncia de um caractere em uma String. O m�todo
		pesquisa a partir do final da String para o in�cio. Se encontrar o caractere, o m�todo retorna o �ndice do caractere na String
		� do contr�rio, retorna �1. H� duas vers�es do lastIndexOf que pesquisam caracteres em uma String. A express�o na linha 20
		utiliza a vers�o que aceita a representa��o de inteiro do caractere. A express�o na linha 22 utiliza a vers�o que aceita dois argumentos
		inteiros � a representa��o de inteiro do caractere e o �ndice a partir do qual iniciar pesquisa de tr�s para a frente */
		System.out.printf("Last 'c' is located at index %d%n", letters.lastIndexOf('c'));
		System.out.printf("Last 'a' is located at index %d%n", letters.lastIndexOf('a', 25));
		System.out.printf("Last '$' is located at index %d%n%n", letters.lastIndexOf('$'));

		// testa indexOf para localizar uma substring em uma string
		System.out.printf("\"def\" is located at index %d%n", letters.indexOf("def"));
		System.out.printf("\"def\" is located at index %d%n", letters.indexOf("def", 7));
		System.out.printf("\"hello\" is located at index %d%n%n", letters.indexOf("hello"));

		// testa lastIndexOf para localizar uma substring em uma string
		System.out.printf("Last \"def\" is located at index %d%n", letters.lastIndexOf("def"));
		System.out.printf("Last \"def\" is located at index %d%n", letters.lastIndexOf("def", 25));
		System.out.printf("Last \"hello\" is located at index %d%n", letters.lastIndexOf("hello"));
	}
	
} // fim da classe StringIndexMethods