package br.com.deitel.capitulo14;

/**
 * @author Gabriel Hideki
 * 
 * Figura 14.7: StringConcatenation.java 
 * M�todo string concat.
 */
public class StringConcatenation {

	public static void main(String[] args) {
		String s1 = "Happy ";
		String s2 = "Birthday";
		
		/* O m�todo String concat (Figura 14.7) concatena dois objetos String (semelhante a usar o operador +) 
		 e retorna um novo objeto String, que cont�m os caracteres das duas Strings originais */
		System.out.printf("s1 = %s%ns2 = %s%n%n", s1, s2);
		System.out.printf("Result of s1.concat(s2) = %s%n", s1.concat(s2));
		System.out.printf("s1 after concatenation = %s%n", s1);
	}
	
} // fim da classe StringConcatenation
