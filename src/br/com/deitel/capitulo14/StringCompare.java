package br.com.deitel.capitulo14;

/**
 * @author Gabriel Hideki
 * 
 * Figura 14.3: StringCompare.java M�todos 
 * String equals, equalsIgnoreCase, compareTo e regionMatches.
 */
public class StringCompare {

	public static void main(String[] args) {
		String s1 = new String("hello"); // s1 � uma c�pia de "hello"
		String s2 = "goodbye";
		String s3 = "Happy Birthday";
		String s4 = "happy birthday";

		System.out.printf("s1 = %s%ns2 = %s%ns3 = %s%ns4 = %s%n%n", s1, s2, s3, s4);

		// teste para igualdade
		if (s1.equals("hello")) // true
			System.out.println("s1 equals \"hello\"");
		else
			System.out.println("s1 does not equal \"hello\"");

		// testa quanto � igualdade com ==
		if (s1 == "hello") // false; eles n�o s�o os mesmos objetos
			System.out.println("s1 is the same object as \"hello\"");
		else
			System.out.println("s1 is not the same object as \"hello\"");

		// testa quanto � igualdade (ignora mai�sculas e min�sculas)
		if (s3.equalsIgnoreCase(s4)) // true
			System.out.printf("%s equals %s with case ignored%n", s3, s4);
		else
			System.out.println("s3 does not equal s4");

		// testa compareTo
		// compareTo retorna 0 se as Strings forem iguais, um n�mero negativo se a String que 
		// invoca compareTo for menor que a String que � passada como um argumento e um n�mero
		// positivo se a String que invoca compareTo for maior que a String que � passada como um argumento. 
		System.out.printf("%ns1.compareTo(s2) is %d", s1.compareTo(s2));
		System.out.printf("%ns2.compareTo(s1) is %d", s2.compareTo(s1));
		System.out.printf("%ns1.compareTo(s1) is %d", s1.compareTo(s1));
		System.out.printf("%ns3.compareTo(s4) is %d", s3.compareTo(s4));
		System.out.printf("%ns4.compareTo(s3) is %d%n%n", s4.compareTo(s3));

		// testa regionMatches (distingue mai�sculas e min�sculas)
		// O m�todo String regionMatches compara a igualdade entre partes de duas Strings. 
		// O primeiro argumento para essa vers�o do m�todo � o �ndice inicial na String que chama o m�todo. 
		//O segundo argumento � uma compara��o de String. O terceiro argumento � o �ndice inicial na compara��o de String. 
		// O �ltimo argumento � o n�mero de caracteres a comparar entre as duas Strings. O m�todo retorna true apenas se 
		// o n�mero especificado de caracteres for lexicograficamente igual.
		// Por fim, a condi��o na linha 62 utiliza uma vers�o de cinco argumentos do m�todo String regionMatches para comparar a
		// igualdade de partes de duas Strings. Quando o primeiro argumento � true, o m�todo ignora mai�sculas e min�sculas dos caracteres
		if (s3.regionMatches(0, s4, 0, 5))
			System.out.println("First 5 characters of s3 and s4 match");
		else
			System.out.println("First 5 characters of s3 and s4 do not match");

		// testa regionMatches (ignora mai�sculas e min�sculas)
		if (s3.regionMatches(true, 0, s4, 0, 5))
			System.out.println("First 5 characters of s3 and s4 match with case ignored");
		else
			System.out.println("First 5 characters of s3 and s4 do not match");
	}
	
} // fim da classe StringCompare
