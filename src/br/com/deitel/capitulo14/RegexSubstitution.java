package br.com.deitel.capitulo14;

import java.util.Arrays;

/**
 * @author Gabriel Hideki
 * 
 * Figura 14.23: RegexSubstitution.Java
 * M�todos string replaceFirst, replaceAll e split.
 */
public class RegexSubstitution {

	public static void main(String[] args) {

		String firstString = "This sentence ends in 5 stars *****";
		String secondString = "1, 2, 3, 4, 5, 6, 7, 8";

		System.out.printf("Original String 1: %s%n", firstString);

		// substitui todas as ocorr�ncias de '*' por '^'
		firstString = firstString.replaceAll("\\*", "^");

		System.out.printf("^ substituted for *: %s%n", firstString);

		// substitui asteriscos por circunflexos
		firstString = firstString.replaceAll("stars", "carets");

		System.out.printf("\"carets\" substituted for \"stars\": %s%n", firstString);

		// substitui palavras por 'palavra'
		System.out.printf("Every word replaced by \"word\": %s%n%n", firstString.replaceAll("\\w+", "word"));

		System.out.printf("Original String 2: %s%n", secondString);

		// substitui os primeiros tr�s d�gitos pelo 'd�gito'
		for (int i = 0; i < 3; i++)
			/* Strings do Java s�o imut�veis, portanto o m�todo replaceFirst retorna uma nova String 
			   em que os caracteres apropriados foram substitu�dos. Essa linha recebe a String original
			   e a substitui pela String retornada por replaceFirst */
			secondString = secondString.replaceFirst("\\d", "digit");

		System.out.printf("First 3 digits replaced by \"digit\" : %s%n", secondString);

		System.out.print("String split at commas: ");
		String[] results = secondString.split(",\\s*"); // divide em v�rgulas
		System.out.println(Arrays.toString(results));
		
		String url = "/credito/consignado/inicia_jornada/prestamista";
		url = url.replaceAll("inicia_jornada", "teste");
		System.out.println(url);
	}

} // fim da classe RegexSubstitution