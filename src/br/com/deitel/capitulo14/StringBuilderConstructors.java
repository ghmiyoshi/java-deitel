package br.com.deitel.capitulo14;

/**
 * @author Gabriel Hideki
 * 
 * Figura 14.10: StringBuilderConstructors.java
 * Construtores StringBuilder.
 */
public class StringBuilderConstructors {
	
	public static void main(String[] args) {
		/* A linha 18 utiliza o construtor sem argumento StringBuilder cria um StringBuilder sem caracteres 
		  e uma capacidade inicial de 16 caracteres (o padr�o para um StringBuilder). A linha 19 utiliza o 
		  construtor StringBuilder que aceita um argumento inteiro para criar um StringBuilder sem caracteres 
		  e a capacidade inicial especificada pelo argumento inteiro (isto �, 10). A linha 10 utiliza o construtor
		  StringBuilder, que aceita um argumento String para criar um StringBuilder contendo o caractere no argumento String. 
		  A capacidade inicial � o n�mero de caracteres no argumento String mais 16. */
		StringBuilder buffer1 = new StringBuilder();
		StringBuilder buffer2 = new StringBuilder(10);
		StringBuilder buffer3 = new StringBuilder("hello");

		System.out.printf("buffer1 = \"%s\"%n", buffer1);
		System.out.printf("buffer2 = \"%s\"%n", buffer2);
		System.out.printf("buffer3 = \"%s\"%n", buffer3);
	}

} // fim da classe StringBuilderConstructors
