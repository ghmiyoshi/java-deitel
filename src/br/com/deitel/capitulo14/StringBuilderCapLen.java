package br.com.deitel.capitulo14;

/**
 * @author Gabriel Hideki
 * 
 * Figura 14.11: StringBuilderCapLen.java
 * M�todos StringBuilder length, setLength, capacity e ensureCapacity.
 */
public class StringBuilderCapLen {

	public static void main(String[] args) {
		/* A classe StringBuilder fornece os m�todos length e capacity para retornar o n�mero de caracteres atualmente em um
		   StringBuilder e o n�mero de caracteres que pode ser armazenado em um StringBuilder sem alocar mais mem�ria, respectivamente.
		   O m�todo ensureCapacity garante que um StringBuilder tenha pelo menos a capacidade especificada. O m�todo setLength aumenta 
		   ou diminui o comprimento de uma StringBuilder */
		StringBuilder buffer = new StringBuilder("Hello, how are you?");

		System.out.printf("buffer = %s%nlength = %d%ncapacity = %d%n%n", buffer.toString(), buffer.length(),
				buffer.capacity());

		buffer.ensureCapacity(75);
		System.out.printf("New capacity = %d%n%n", buffer.capacity());

		buffer.setLength(10);
		System.out.printf("New length = %d%nbuffer = %s%n", buffer.length(), buffer.toString());
	}

} // fim da classe StringBuilderCapLen