package br.com.deitel.capitulo16;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.deitel.capitulo8.Time1;

/**
 * @author Gabriel Hideki
 * 
 * Figura 16.9: Sort3.java
 * M�todo sort de Collections com um objeto Comparator personalizado.
 */
public class Sort3 {
	
	public static void main(String[] args) {
		List<Time1> list = new ArrayList<>(); // cria List

		list.add(new Time1(6, 24, 34));
		list.add(new Time1(18, 14, 58));
		list.add(new Time1(6, 05, 34));
		list.add(new Time1(12, 14, 58));
		list.add(new Time1(6, 24, 22));

		// gera sa�da de elementos List
		System.out.printf("Unsorted array elements:%n%s%n", list);

		// classifica em ordem utilizando um comparador
		//list.sort(new TimeComparator());
		Collections.sort(list, new TimeComparator());

		// gera sa�da de elementos List
		System.out.printf("Sorted list elements:%n%s%n", list);
	}
	
} // fim da classe Sort3
