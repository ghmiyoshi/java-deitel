package br.com.deitel.capitulo16;

import java.util.Comparator;

import br.com.deitel.capitulo8.Time1;

public class TimeComparator implements Comparator<Time1> {
	
	@Override
	public int compare(Time1 time1, Time1 time2) {
		int hourDifference = time1.getHour() - time2.getHour();

		if (hourDifference != 0) // testa a primeira hora
			return hourDifference;

		int minuteDifference = time1.getMinute() - time2.getMinute();

		if (minuteDifference != 0) // ent�o testa o minuto
			return minuteDifference;

		int secondDifference = time1.getSecond() - time2.getSecond();
		return secondDifference;
	}
} // fim da classe TimeComparator