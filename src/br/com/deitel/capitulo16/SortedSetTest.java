package br.com.deitel.capitulo16;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Gabriel Hideki
 * 
 * Figura 16.17: SortedSetTest.java
 * Usando SortedSets e TreeSets.
 */
public class SortedSetTest {
	
	public static void main(String[] args) {
		// cria TreeSet a partir do array colors
		String[] colors = { "yellow", "green", "black", "tan", "grey", "white", "orange", "red", "green" };
		SortedSet<String> tree = new TreeSet<>(Arrays.asList(colors));

		System.out.print("sorted set: ");
		printSet(tree);

		// obt�m headSet com base em "orange"
		System.out.print("headSet (\"orange\"): ");
		printSet(tree.headSet("orange"));

		// obt�m tailSet baseado em "orange"
		System.out.print("tailSet (\"orange\"): ");
		printSet(tree.tailSet("orange"));
		// obt�m primeiro e �ltimo elementos
		System.out.printf("first: %s%n", tree.first());
		System.out.printf("last : %s%n", tree.last());
	}

	// envia SortedSet para a sa�da usando a instru��o for aprimorada
	private static void printSet(SortedSet<String> set) {
		for (String s : set)
			System.out.printf("%s ", s);

		System.out.println();
	}
	
} // fim da classe SortedSetTest