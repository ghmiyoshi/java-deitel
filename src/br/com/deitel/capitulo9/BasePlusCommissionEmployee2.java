package br.com.deitel.capitulo9;

/**
 * @author Gabriel Hideki
 * 
 * Figura 9.8: BaseplusCommissionEmployee.java 
 * Membros private da superclasse n�o podem ser acessados em uma subclasse.
 */
public class BasePlusCommissionEmployee2 extends CommissionEmployee {

	private double baseSalary; // sal�rio-base por semana

	// construtor de seis argumentos
	public BasePlusCommissionEmployee2(String firstName, String lastName, String socialSecurityNumber,
			double grossSales, double commissionRate, double baseSalary) {
		super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);

		// se baseSalary � inv�lido, lan�a uma exce��o
		if (baseSalary < 0.0)
			throw new IllegalArgumentException("Base salary must be >= 0.0");
	}

	// configura o sal�rio-base
	public void setBaseSalary(double baseSalary) {
		if (baseSalary < 0.0)
			throw new IllegalArgumentException("Base salary must be >= 0.0");

		this.baseSalary = baseSalary;
	}

	// retorna o sal�rio-base
	public double getBaseSalary() {
		return baseSalary;
	}

	// calcula os lucros
	@Override // indica que esse m�todo substitui um m�todo da superclasse
	public double earnings() {
		// n�o permitido: commissionRate e grossSales privado em superclasse
		return baseSalary + (commissionRate * grossSales);
	}

	// retorna a representa��o de String de BasePlusCommissionEmployee
	@Override
	public String toString() {
		return String.format("%s: %s %s%n%s: %s%n%s: %.2f%n%s: %.2f%n%s: %.2f", "base-salaried commission employee",
				firstName, lastName, "social security number", socialSecurityNumber, "gross sales", grossSales,
				"commission rate", commissionRate, "base salary", baseSalary);
	}
	
} // fim da classe BasePlusCommissionEmployee
