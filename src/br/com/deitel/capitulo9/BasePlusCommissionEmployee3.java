package br.com.deitel.capitulo9;

/**
 * @author Gabriel Hideki
 * 
 * Figura 9.11: BasePlusCommissionEmployee.java
 * A classe BasePlusCommissionEmployee � herdada de CommissionEmployee
 * e acessa os dados private da superclasse via m�todos public herdados.
 */
public class BasePlusCommissionEmployee3 extends CommissionEmployee2 {

	private double baseSalary; // sal�rio-base por semana

	// construtor de seis argumentos
	public BasePlusCommissionEmployee3(String firstName, String lastName, String socialSecurityNumber,
			double grossSales, double commissionRate, double baseSalary) {
		super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);

		// se baseSalary � inv�lido, lan�a uma exce��o
		if (baseSalary < 0.0)
			throw new IllegalArgumentException("Base salary must be >= 0.0");

		this.baseSalary = baseSalary;
	}

	// configura o sal�rio-base
	public void setBaseSalary(double baseSalary) {
		if (baseSalary < 0.0)
			throw new IllegalArgumentException("Base salary must be >= 0.0");

		this.baseSalary = baseSalary;
	}

	// retorna o sal�rio-base
	public double getBaseSalary() {
		return baseSalary;
	}

	// calcula os lucros
	@Override
	public double earnings() {
		return getBaseSalary() + super.earnings();
	}

	// retorna a representa��o de String de BasePlusCommissionEmployee
	@Override
	public String toString() {
		return String.format("%s %s%n%s: %.2f", "base-salaried", super.toString(), "base salary", getBaseSalary());
	}
	
	
	public void teste() {
		
	}

} // fim da classe BasePlusCommissionEmployee
