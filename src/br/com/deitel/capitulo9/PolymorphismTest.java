package br.com.deitel.capitulo9;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.1: PolymorphismTest.java
 * Atribuindo refer�ncias de superclasse e subclasse a vari�veis de superclasse e de subclasse.
 */
public class PolymorphismTest {
	
	public static void main(String[] args) {
		// atribui uma refer�ncia de superclasse � vari�vel de superclasse
		CommissionEmployee2 commissionEmployee = new CommissionEmployee2("Sue", "Jones", "222-22-2222", 10000, .06);

		// atribui uma refer�ncia de subclasse � vari�vel de subclasse
		BasePlusCommissionEmployee3 basePlusCommissionEmployee = new BasePlusCommissionEmployee3("Bob", "Lewis", "333-33-3333", 5000, .04, 300);
		
		// invoca toString no objeto de superclasse utilizando a vari�vel de superclasse
		System.out.printf("%s %s:%n%n%s%n%n", "Call CommissionEmployee's toString with superclass reference ",
				"to superclass object", commissionEmployee.toString());

		// invoca toString no objeto de subclasse utilizando a vari�vel de subclasse
		System.out.printf("%s %s:%n%n%s%n%n", "Call BasePlusCommissionEmployee's toString with subclass",
				"reference to subclass object", basePlusCommissionEmployee.toString());

		/* Quando uma vari�vel de superclasse cont�m uma refer�ncia a um objeto de subclasse, e essa refer�ncia � utilizada para chamar um m�todo, 
		   a vers�o da subclasse do m�todo � chamada. Da�, commissionEmployee2.toString() na verdade chama o m�todo toString da classe BasePlusCommissionEmployee. 
		   O compilador Java permite esse �cruzamento� porque um objeto de uma subclasse � um objeto da sua superclasse (mas n�o vice-versa). 
		   Quando o compilador encontra uma chamada de m�todo feita por meio de uma vari�vel, ele determina se o m�todo pode ser chamado verificando o tipo de 
		   classe da vari�vel. Se essa classe cont�m a declara��o adequada de m�todo (ou herda um), a chamada � compilada. Em tempo de execu��o, o tipo do objeto 
		   que a vari�vel referencia determina o m�todo real a utilizar.*/
		CommissionEmployee2 commissionEmployee2 = basePlusCommissionEmployee;
		System.out.printf("%s %s:%n%n%s%n", "Call BasePlusCommissionEmployee's toString with superclass",
				"reference to subclass object", commissionEmployee2.toString());
	} // fim de main
	
} // fim da classe PolymorphismTest
