package br.com.deitel.capitulo11;

import java.util.Scanner;

/**
 * @author Gabriel Hideki
 * 
 * Figura 11.2: DivideByZeroNoExceptionHandling.java
 * Divis�o de inteiro sem tratamento de exce��o.
 * 
 */
public class DivideByZeroNoExceptionHandling {
	// demonstra o lan�amento de uma exce��o quando ocorre uma divis�o por zero
	public static int quotient(int numerator, int denominator) {
		return numerator / denominator; // poss�vel divis�o por zero
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Please enter an integer numerator: ");
		int numerator = scanner.nextInt();
		System.out.print("Please enter an integer denominator: ");
		int denominator = scanner.nextInt();

		int result = quotient(numerator, denominator);
		System.out.printf("%nResult: %d / %d = %d%n", numerator, denominator, result);
		scanner.close();
	}

} // fim da classe DivideByZeroNoExceptionHandling
