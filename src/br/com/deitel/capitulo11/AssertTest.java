package br.com.deitel.capitulo11;

import java.util.Scanner;

/**
 * @author Gabriel Hideki
 * 
 * Figura 11.8: AssertTest.java
 * Verificando com assert se um valor est� dentro do intervalo
 * 
 */
public class AssertTest {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.print("Enter a number between 0 and 10: ");
		int number = input.nextInt();

		// afirma que o valor � >= 0 e <= 10
		assert (number >= 0 && number <= 10) : "bad number: " + number;

		System.out.printf("You entered %d%n", number);
		input.close();
	}
	
} // fim da classe AssertTest