package br.com.deitel.capitulo4;

import java.util.Scanner;

/**
 * @author Gabriel Hideki
 *
 * Figura 4.10: ClassAverage.java
 * Resolvendo o problema da m�dia da classe usando a repeti��o controlada por sentinela.
 */
public class ClassAverage2 {
	
	public static void main(String[] args) {
		
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);
		
		// fase de inicializa��o
		int total = 0; // inicializa a soma das notas inseridas pelo usu�rio
		int gradeCounter = 0; // inicializa n� da nota a ser inserido em seguida
		
		// fase de processamento
		// solicita entrada e l� a nota do usu�rio
		System.out.print("Enter grade or -1 to quit:");
		int grade = input.nextInt();
		
		// faz um loop at� ler o valor de sentinela inserido pelo usu�rio
		while(grade != -1) {
			total += grade; // adiciona grade a total
			gradeCounter++; // incrementa counter
				
			// solicita entrada e l� a pr�xima nota fornecida pelo usu�rio
			System.out.print("Enter grade or -1 to quit: ");
			grade = input.nextInt(); // insere a pr�xima nota
		}
		
		// fase de t�rmino, se usu�rio inseriu pelo menos uma nota...
		if (gradeCounter != 0) {
			/* usa n�mero com ponto decimal para calcular m�dia das notas.
			   (double) convers�o expl�cita - os valores int e double, s�o promovidos
			   para valores double para uso na express�o.
			   Nesse exemplo, o valor de gradeCounter � promovido para o tipo double, 
			   ent�o a divis�o de ponto flutuante � realizada e o resultado do c�lculo �
			   atribu�do a average. */
			double average = (double) total / gradeCounter;

			// exibe o total e a m�dia (com dois d�gitos de precis�o)
			System.out.printf("%nTotal of the %d grades entered is %d%n", gradeCounter, total);
			System.out.printf("Class average is %.2f%n", average);
		} else {
			// nenhuma nota foi inserida, assim gera a sa�da da mensagem apropriada
			System.out.println("No grades were entered");
		}
		
		input.close();
	} 

} // fim da classe ClassAverage