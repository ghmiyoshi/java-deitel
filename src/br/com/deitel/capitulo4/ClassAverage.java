package br.com.deitel.capitulo4;

import java.util.Scanner;

/**
 * @author Gabriel Hideki
 *
 * Figura 4.8: ClassAverage.java
 * Resolvendo o problema da m�dia da classe usando a repeti��o controlada por contador.
 */
public class ClassAverage {
	
	public static void main(String[] args) {
		
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);
		
		// fase de inicializa��o
		int total = 0; // inicializa a soma das notas inseridas pelo usu�rio
		int gradeCounter = 0; // inicializa n� da nota a ser inserido em seguida
		
		System.out.print("Enter number of students: ");
		int students = input.nextInt();
		
		// fase de processamento utiliza repeti��o controlada por contador
		while(gradeCounter < students) { // faz o loop 10 vezes
			System.out.print("Enter grade: "); // prompt
			int grade = input.nextInt(); // insere a pr�xima nota
			
			total += grade; // adiciona grade a total
			
			gradeCounter++; // incrementa o contador por 1
		}
		
		// fase de t�rmino
		int average = total / gradeCounter; // divis�o de inteiros produz um resultado inteiro
		
		// exibe o total e a m�dia das notas
		System.out.printf("%nTotal of all 10 grades is %d%n", total);
		System.out.printf("Class average is %d%n", average);
		
		input.close();
	} 

} // fim da classe ClassAverage