package br.com.deitel.capitulo4;

import java.util.Scanner;

/**
 * @author Gabriel Hideki
 * 
 * Figura 4.12: Analysis.java
 * An�lise dos resultados do exame utilizando instru��es de controle aninhadas.
 */
public class Analysis {

	public static void main(String[] args) {
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);

		// inicializando vari�veis nas declara��es
		int passes = 0;
		int failures = 0;
		int studentCounter = 1;

		// processa 10 alunos utilizando o loop controlado por contador
		while (studentCounter <= 10) {
			// solicita ao usu�rio uma entrada e obt�m valor fornecido pelo usu�rio
			System.out.print("Enter result (1 = pass, 2 = fail): ");
			int resultado = input.nextInt();

			// if...else est� aninhado na instru��o while
			if (resultado != 1 && resultado != 2) {
				System.out.println("Somente � aceito 1 e 2 como resultado");
				break;
			} else if (resultado == 1) {
				passes++;
			} else {
				failures++;
			}

			// incrementa studentCounter at� o loop terminar
			studentCounter++;
		}

		// fase de t�rmino; prepara e exibe os resultados
		System.out.printf("Passed: %d%nFailed: %d%n", passes, failures);
		
		// determina se mais de 8 alunos foram aprovados
		if (passes > 8) {
			System.out.println("Bonus to instructor!");
		}

		input.close();
	}

} // fim da classe Analysis
