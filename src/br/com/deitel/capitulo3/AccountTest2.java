package br.com.deitel.capitulo3;

/**
 * @author Gabriel Hideki
 *
 * Figura 3.6: AccountTest.Java
 * Usando o construtor de Account para inicializar a inst�ncia name vari�vel no momento em que cada objeto Account � criado.
 */
public class AccountTest2 {
	
	public static void main(String[] args) {
		// cria dois objetos Account
		Account account1 = new Account("Jane Green", 1.01);
		Account account2 = new Account("John Blue", 2.02);

		// exibe o valor inicial de nome para cada Account
		System.out.printf("account1 name is: %s balance is: R$ %.2f%n", account1.getName(), account1.getBalance());
		System.out.printf("account2 name is: %s balance is: R$ %.2f%n", account2.getName(), account2.getBalance());
	} // fim de main

} // fim da classe AccountTest2