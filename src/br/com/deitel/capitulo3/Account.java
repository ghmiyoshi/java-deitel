package br.com.deitel.capitulo3;

/**
 * @author Gabriel Hideki
 * 
 * Figura 3.1: Account.java
 * Classe Account que cont�m uma vari�vel de inst�ncia name e m�todos para configurar e obter seu valor.
 */
public class Account {

	private String name; // vari�vel de inst�ncia
	private double balance; // vari�vel de inst�ncia

	// m�todo para recuperar o nome do objeto
	public String getName() {
		return name; // retorna valor do nome para o chamador
	}

	// m�todo para definir o nome no objeto
	public void setName(String name) {
		this.name = name; // armazena o nome
	}
	
	// o construtor inicializa name com nome do par�metro
	public Account(String name, double balance) { // o nome do construtor � nome da classe  
		this.name = name; // atribui name � vari�vel de inst�ncia name
		
		/* valida se o balance � maior que 0.0; se n�o for,
		 a vari�vel de inst�ncia balance mant�m seu valor inicial padr�o de 0.0 */
		if(balance > 0.0) {
			this.balance = balance;
		}
	}
	
	// m�todo que deposita (adiciona) apenas uma quantia v�lida no saldo
	public void deposit(double depositAmount) {
		if(depositAmount > 0.0) { // se depositAmount for v�lido
			this.balance += depositAmount; // o adiciona ao saldo  
		}
	}
	
	// m�todo retorna o saldo da conta
	public double getBalance() {
		return balance;
	}

} // fim da classe Account