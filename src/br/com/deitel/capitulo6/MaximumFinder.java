package br.com.deitel.capitulo6;

import java.util.Scanner;

/**
 * @author Gabriel Hideki
 *
 * Figura 6.3: MaximumFinder.java
 * M�todo maximum declarado pelo programador com tr�s par�metros double.
 */
public class MaximumFinder {

	// obt�m tr�s valores de ponto flutuante e localiza o valor m�ximo
	public static void main(String[] args) {
		// cria Scanner para entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);

		double number1, number2, number3;

		// solicita e insere tr�s valores de ponto flutuante
		System.out.println("Enter three floating-point values separated by spaces: ");
		number1 = input.nextInt(); // l� o primeiro double
		number2 = input.nextInt(); // l� o segundo double
		number3 = input.nextInt(); // l� o terceiro double

		// determina o valor m�ximo
		double result = maximum(number1, number2, number3);

		// exibe o valor m�ximo
		System.out.println("Maximum is: " + result);

		input.close();
	}

	// retorna o m�ximo dos seus tr�s par�metros de double
	public static double maximum(double number1, double number2, double number3) {
//		double maximumValue = number1; // sup�e que x � o maior valor inicial
//
//		// determina se y � maior que maximumValue
//		if (number2 > number1) {
//			maximumValue = number2;
//		}
//
//		// determina se z � maior que maximumValue
//		if (number3 > number1) {
//			maximumValue = number3;
//		}
//
//		return maximumValue;
		return Math.max(number3, Math.max(number1, number2));
	}

} // fim da classe MaximumFinder
