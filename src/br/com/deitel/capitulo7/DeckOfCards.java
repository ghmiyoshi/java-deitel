package br.com.deitel.capitulo7;

import java.security.SecureRandom;

/**
 * @author Gabriel Hideki
 * 
 * Figura 7.10: DeckOfCards.java
 * Classe DeckOfCards representa um baralho.
 * 
 */
public class DeckOfCards {

	private Card[] deck; // array de objetos Card
	private int currentCard; // �ndice da pr�xima Card a ser distribu�da (0-51)
	private static final int NUMBER_OF_CARDS = 52; // n�mero constante de Cards
	private static final SecureRandom randomNumbers = new SecureRandom(); // gerador de n�mero aleat�rio

	// construtor preenche baralho de cartas
	public DeckOfCards() {
		String[] faces = { "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
		String[] suits = { "Hearts", "Diamonds", "Clubs", "Spades" };

		this.deck = new Card[NUMBER_OF_CARDS]; // cria array de objetos Card
		this.currentCard = 0; // a primeira Card distribu�da ser� o deck[0]

		// preenche baralho com objetos Card
		for (int count = 0; count < deck.length; count++) {
			// count % 13 sempre resulta em um valor de 0 a 12 (os 13 �ndices do array faces nas linhas 22) 
			// count / 13 sempre resulta em um valor de 0 a 3 (os quatro �ndices do array suits na linha 23)
			deck[count] = new Card(faces[count % 13], suits[count / 13]); 
		}
	}

	// embaralha as cartas com um algoritmo de uma passagem
	public void shuffle() {
		// a pr�xima chamada para o m�todo dealCard deve come�ar no deck[0] novamente
		currentCard = 0;

		// para cada Card, seleciona outra Card aleat�ria (0-51) e as compara
		for (int first = 0; first < deck.length; first++) {
			// seleciona um n�mero aleat�rio entre 0 e 51
			int second = randomNumbers.nextInt(NUMBER_OF_CARDS);

			/* compara Card atual com Card aleatoriamente selecionada
			   Se deck[first] for "Ace" de "Spades" e deck[second] for "Queen" de "Hearts", depois da primeira atribui��o, ambos os
			   elementos do array conter�o "Queen" de "Hearts", e o "Ace" de "Spades" ser� perdido � da� a vari�vel extra temp ser necess�ria. */
			Card temp = deck[first];
			deck[first] = deck[second];
			deck[second] = temp;
		}
	}

	// distribui uma Card
	public Card dealCard() {
		// determina se ainda h� Cards a serem distribu�das
		if (currentCard < deck.length)
			return deck[currentCard++]; // retorna Card atual no array
		else
			return null; // retorna nulo para indicar que todos as Cards foram distribu�das
	}
	
} // fim da classe DeckOfCards
