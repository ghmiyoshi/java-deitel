package br.com.deitel.capitulo7;

/**
 * @author Gabriel Hideki
 * 
 * Figura 7.3: InitArray.java
 * Inicializando os elementos de um array com um inicializador de array.
 */
public class InitArray2 {

	public static void main(String[] args) {
		// A lista de inicializador especifica o valor inicial de cada elemento
		int[] array = { 32, 27, 64, 18, 95, 14, 90, 70, 60, 37 };

		System.out.printf("%s%8s%n", "Index", "Value"); // t�tulos de coluna

		// gera sa�da do valor de cada elemento do array
		for (int count = 0; count < array.length; count++) {
			System.out.printf("%5s%8s%n", count, array[count]);
		}
	}

} // fim da classe InitArray
