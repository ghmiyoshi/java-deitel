package br.com.deitel.capitulo7;

import java.util.Arrays;

/**
 * @author Gabriel Hideki
 * 
 * Figura 7.22: ArrayManipulations.java 
 * M�todos da classe Arrays e System.arraycopy.
 */
public class ArrayManipulations {
	
	public static void main(String[] args) {
		// classifica doubleArray em ordem crescente
		double[] doubleArray = { 8.4, 9.3, 0.2, 7.9, 3.4 };
		Arrays.sort(doubleArray);
		System.out.printf("doubleArray: ");

		for (double value : doubleArray)
			System.out.printf("%.1f ", value);

		// preenche o array de 10 elementos com 7
		int[] filledIntArray = new int[10];
		Arrays.fill(filledIntArray, 7); // preenche todos os 10 elementos de filledIntArray com 7.
		displayArray(filledIntArray, "filledIntArray");

		// copia array intArray em array intArrayCopy
		int[] intArray = { 1, 2, 3, 4, 5, 6 };
		int[] intArrayCopy = new int[intArray.length];
		
		/* System arraycopy � o array a partir do qual os elementos s�o copiados. O segundo argumento (0) � o �ndice que especifica o ponto
		   inicial no intervalo de elementos a copiar a partir do array. Esse valor pode ser qualquer �ndice de array v�lido. O terceiro argumento
		   (intArrayCopy) especifica o array de destino que armazenar� a c�pia. O quarto argumento (0) especifica o �ndice no array de
		   destino em que o primeiro elemento copiado deve ser armazenado. O �ltimo argumento especifica o n�mero de elementos a ser
		   copiado a partir do array no primeiro argumento. Nesse caso, copiamos todos os elementos no array. */
		System.arraycopy(intArray, 0, intArrayCopy, 0, intArray.length);
		displayArray(intArray, "intArray");
		displayArray(intArrayCopy, "intArrayCopy");

		/* Determina se todos os elementos de dois arrays s�o equivalentes. 
		   Se os arrays contiverem os mesmos elementos na mesma ordem, o m�todo retorna true, 
		   caso contr�rio, retorna false.
		   verifica a igualdade de intArray e intArrayCopy */
		boolean b = Arrays.equals(intArray, intArrayCopy);
		System.out.printf("%n%nintArray %s intArrayCopy%n", (b ? "==" : "!="));

		// verifica a igualdade de intArray e filledIntArray
		b = Arrays.equals(intArray, filledIntArray);
		System.out.printf("intArray %s filledIntArray%n", (b ? "==" : "!="));

		/* Realiza uma pesquisa bin�ria em intArray, usando o segundo argumento (5 e 8763, respectivamente) 
		   como a chave. Se value for encontrado, binarySearch retorna o �ndice do elemento,
		   caso contr�rio, binarySearch retorna um valor negativo.
		   pesquisa o valor 5 em intArray */
		int location = Arrays.binarySearch(intArray, 5);

		if (location >= 0)
			System.out.printf("Found 5 at element %d in intArray%n", location);
		else
			System.out.println("5 not found in intArray");

		// pesquisa o valor 8763 em intArray
		location = Arrays.binarySearch(intArray, 8763);

		if (location >= 0)
			System.out.printf("Found 8763 at element %d in intArray%n", location);
		else
			System.out.println("8763 not found in intArray");
	}

	// gera sa�da de valores em cada array
	public static void displayArray(int[] array, String description) {
		System.out.printf("%n%s: ", description);

		for (int value : array)
			System.out.printf("%d ", value);
	}
	
} // fim da classe ArrayManipulations
