package br.com.deitel.capitulo7;

import java.security.SecureRandom;

/**
 * @author Gabriel Hideki
 * 
 * Figura 7.7: RollDie.java
 * Programa de jogo de dados utilizando arrays em vez de switch.
 */
public class RollDie {

	public static void main(String[] args) {
		SecureRandom randomNumbers = new SecureRandom();
		int[] frequency = new int[7]; // array de contadores de frequ�ncia

		// lan�a o dado 6.000.000 vezes; usa o valor do dado como �ndice de frequ�ncia
		for (int roll = 1; roll <= 6000000; roll++) {
			// 0 1 2 3 4 5
			++frequency[1 + randomNumbers.nextInt(6)]; // utiliza o valor aleat�rio para determinar qual elemento frequency incrementar durante cada itera��o do loop
		}

		// gera sa�da do valor de cada elemento do array
		for (int face = 1; face < frequency.length; face++) {
			System.out.printf("%4d%10d%n", face, frequency[face]);
		}
	}

} // fim da classe RollDie
