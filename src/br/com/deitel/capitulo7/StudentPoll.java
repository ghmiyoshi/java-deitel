package br.com.deitel.capitulo7;

import java.security.SecureRandom;

/**
 * @author Gabriel Hideki
 * 
 * Figura 7.8: StudentPoll.java
 * Programa de an�lise de enquete.
 */
public class StudentPoll {

	public static void main(String[] args) {
		SecureRandom integerRandom = new SecureRandom();

		// array das respostas dos alunos (mais tipicamente, inserido em tempo de execu��o)
		int[] responses = new int[20];
		int[] frequency = new int[6]; // array de contadores de frequ�ncia

		// para cada resposta, seleciona elemento de respostas e utiliza esse valor
		// como �ndice de frequ�ncia para determinar elemento a incrementar
		for (int counter = 0; counter < responses.length; counter++) {
			++responses[1 + integerRandom.nextInt(4)];
		}

		System.out.printf("%s%10s%n", "Rating", "Frequency");
		
		for (int rating = 1; rating < frequency.length; rating++) {
			System.out.printf("%4d%10d%n", rating, responses[rating]);
		}
	}

}
