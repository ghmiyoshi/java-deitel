package br.com.deitel.capitulo7;

import java.time.LocalDateTime;

public class DiferenceFinalAndStaticAndStaticFinal {

	/* Final o valor dessa propriedade n�o pode ser alterado.
	   Se uma vari�vel final n�o � inicializada na sua declara��o ou em cada construtor, 
	   ocorre um erro de compila��o */
	private final LocalDateTime data;
	
	// Static final � uma constante, ter� o mesmo valor para todas as inst�ncias e n�o pode ser alterada
	private static final Long count2 = 10L;
	
	/* Static � uma propriedade com o valor compartilhado em todas as inst�ncias e o seu valor pode ser alterado.
	   Se for alterado, esse valor ser� refletido para todas as inst�ncias criadas e para as novas */
	private static Long count = 0L;

	public DiferenceFinalAndStaticAndStaticFinal() {
		this.data = LocalDateTime.now();
		System.out.println("Final: " + data);
		System.out.println("Static: " + count);
		System.out.println("Static final: " + count2 + "\n");
	}
	
	public LocalDateTime getData() {
		return data;
	}
	
	public static void main(String[] args) {
		System.out.println("Count: " + DiferenceFinalAndStaticAndStaticFinal.count++);
		System.out.println("Count: " + DiferenceFinalAndStaticAndStaticFinal.count2 + "\n");
		System.out.println("Count: " + DiferenceFinalAndStaticAndStaticFinal.count);
		System.out.println("Count: " + DiferenceFinalAndStaticAndStaticFinal.count2);
	}
	
}

