package br.com.deitel.capitulo7;

/**
 * @author Gabriel Hideki
 * 
 * Figura 7.9: Card.java
 * Classe Card representa uma carta de baralho.
 */
public class Card {

	private final String face; // face da carta ("Ace", "Deuce", ...)
	private final String suit; // naipe da carta ("Hearts", "Diamonds", ...)

	// construtor de dois argumentos inicializa face e naipe da carta
	public Card(String cardFace, String cardSuite) {
		this.face = cardFace; // inicializa face da carta
		this.suit = cardSuite; // inicializa naipe da carta
	}

	// retorna representação String de Card
	@Override
	public String toString() {
		return this.face + " of " + this.suit;
	}

} // fim da classe Card
