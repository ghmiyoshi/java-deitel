package br.com.deitel.capitulo17;

/**
 * @author Gabriel Hideki
 * 
 * Figura 17.9: Employee.java
 * Classe Employee.
 */
public class Employee {
	
	private String firstName;
	private String lastName;
	private double salary;
	private String department;

	// construtor
	public Employee(String firstName, String lastName, double salary, String department) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;
		this.department = department;
	}

	// configura firstName
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	// obt�m firstName
	public String getFirstName() {
		return firstName;
	}

	// configura lastName
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	// obt�m lastName
	public String getLastName() {
		return lastName;
	}

	// configura o sal�rio
	public void setSalary(double salary) {
		this.salary = salary;
	}

	// obt�m sal�rio
	public double getSalary() {
		return salary;
	}

	// configura departamento
	public void setDepartment(String department) {
		this.department = department;
	}

	// obt�m departamento
	public String getDepartment() {
		return department;
	}

	// retorna o nome e o sobrenome do empregado combinados
	public String getName() {
		return String.format("%s %s", getFirstName(), getLastName());
	}

	// retorna uma String contendo informa��es do Employee
	@Override
	public String toString() {
		return String.format("%-8s %-8s %8.2f %s", getFirstName(), getLastName(), getSalary(), getDepartment());
	} // fim do m�todo toString
	
} // fim da classe Employee