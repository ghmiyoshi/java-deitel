package br.com.deitel.capitulo8;

/**
 * @author Gabriel Hideki
 * 
 * Figura 8.6: Time2Test.java 
 * Construtores sobrecarregados utilizados para inicializar objetos Time.
 */
public class TimeConstructorOverloadTest {

	public static void main(String[] args) {
		TimeConstructorOverload t1 = new TimeConstructorOverload(); // 00:00:00
		TimeConstructorOverload t2 = new TimeConstructorOverload(2); // 02:00:00
		TimeConstructorOverload t3 = new TimeConstructorOverload(21, 34); // 21:34:00
		TimeConstructorOverload t4 = new TimeConstructorOverload(12, 25, 42); // 12:25:42
		TimeConstructorOverload t5 = new TimeConstructorOverload(t4); // 12:25:42

		System.out.println("Constructed with:");
		displayTime("t1: all default arguments", t1);
		displayTime("t2: hour specified; default minute and second", t2);
		displayTime("t3: hour and minute specified; default second", t3);
		displayTime("t4: hour, minute and second specified", t4);
		displayTime("t5: Time2 object t4 specified", t5);

		// tenta inicializar t6 com valores inválidos
		try {
			TimeConstructorOverload t6 = new TimeConstructorOverload(27, 74, 99); // valores inválidos
			t6.getClass();
		} catch (IllegalArgumentException e) {
			System.out.printf("%nException while initializing t6: %s%n", e.getMessage());
		}
	}

	// exibe um objeto Time2 nos formatos de 24 horas e 12 horas
	private static void displayTime(String header, TimeConstructorOverload t) {
		System.out.printf("%s%n %s%n %s%n", header, t.toUniversalString(), t.toString());
	}

} // fim da classe Time2Test
