package br.com.deitel.capitulo8;

/**
 * @author Gabriel Hideki
 * 
 *         Figura 8.1: Time1.java Declara��o de classe Time1 mant�m a data/hora
 *         no formato de 24 horas.
 */
public class Time1 {

	private int hour; // 0 - 23
	private int minute; // 0 - 59
	private int second; // 0 - 59
	
	public Time1() {
		
	}

	public Time1(int hour, int minute, int second) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	/* configura um novo valor de tempo usando hora universal;
	lan�a uma exce��o se a hora, minuto ou segundo for inv�lido */
	public void setTime(int hour, int minute, int second) {
		// valida hora, minuto e segundo
		if (hour < 0 || hour >= 24 || minute < 0 || minute >= 60 || second < 0 || second >= 60) {
			throw new IllegalArgumentException("Hour, minute and/or second was out of range");
		}

		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	public String toUniversalString() {
		return String.format("%02d:%02d:%02d", hour, minute, second);
	}

	// converte em String no formato padr�o de data/hora (H:MM:SS AM ou PM)
	public String toString() {
		return String.format("%d:%02d:%02d %s", ((hour == 0 || hour == 12) ? 12 : hour % 12), minute, second,
				(hour < 12 ? "AM" : "PM"));
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

} // fim da classe Time1
