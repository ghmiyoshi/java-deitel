package br.com.deitel.capitulo8;

import java.util.EnumSet;

/**
 * @author Gabriel Hideki
 * 
 * Figura 8.11: EnumTest.java
 * Testando o tipo enum Book.
 */
public class BookEnumTest {

	public static void main(String[] args) {
		System.out.println("All books:");

		/* imprime todos os livros em enum Book
		   values() retorna um array das constantes do enum na ordem em que elas foram declaradas
		 */
		for (BookEnum book : BookEnum.values())
			System.out.printf("%-10s%-45s%s%n", book, book.getTitle(), book.getCopyrightYear());

		System.out.printf("%nDisplay a range of enum constants:%n");

		// imprime os primeiros quatro livros
		for (BookEnum book : EnumSet.range(BookEnum.JHTP, BookEnum.CPPHTP))
			System.out.printf("%-10s%-45s%s%n", book, book.getTitle(), book.getCopyrightYear());
	}
	
} // fim da classe EnumTest
