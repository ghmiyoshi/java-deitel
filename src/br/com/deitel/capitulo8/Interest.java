package br.com.deitel.capitulo8;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * @author Gabriel Hideki
 * 
 * Interest.java
 * C�lculos de juros compostos com BigDecimal.
 */
public class Interest {
	public static void main(String args[]) {
		// quantidade principal inicial antes dos juros
		BigDecimal principal = BigDecimal.valueOf(1000.0);
		BigDecimal rate = BigDecimal.valueOf(0.05); // taxa de juros

		// exibe cabe�alhos
		System.out.printf("%s%20s%n", "Year", "Amount on deposit");

		// calcula quantidade de dep�sito para cada um dos dez anos
		for (int year = 1; year <= 10; year++) {
			// calcula nova quantidade durante ano especificado
			BigDecimal amount = principal.multiply(rate.add(BigDecimal.ONE).pow(year));
			// retorna um BigDecimal com dois d�gitos � direita do ponto decimal e usa arredondamento cont�bil
			// amount.setScale(2, RoundingMode.HALF_EVEN);

			/* exibe o ano e a quantidade 
			 getCurrencyInstance() formata valores num�ricos como Strings de moedas espec�ficas da localidade
			 format retorna a representa��o String espec�fica da localidade, arredondada para dois d�gitos 
			 � direita do ponto decimal */
			System.out.printf("%4d%20s%n", year, NumberFormat.getCurrencyInstance().format(amount));
		}
	}
	
} // fim da classe Interest