package br.com.deitel.capitulo10.folhaDePagamento;

import java.math.BigDecimal;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.8: BaseplusCommissionEmployee.java 
 * Classe BasePlusCommissionEmployee estende a CommissionEmployee.
 */
public class BasePlusCommissionEmployee extends CommissionEmployee {

	private BigDecimal baseSalary; // sal�rio de base por semana

	// construtor
	public BasePlusCommissionEmployee(String firstName, String lastName, String socialSecurityNumber, BigDecimal grossSales,
			BigDecimal commissionRate, BigDecimal baseSalary) {
		super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);

		if (baseSalary.intValue() < 0.0) // valida baseSalary
			throw new IllegalArgumentException("Base salary must be >= 0.0");

		this.baseSalary = baseSalary;
	}

	// configura o sal�rio-base
	public void setBaseSalary(BigDecimal baseSalary) {
		if (baseSalary.intValue() < 0.0) // valida baseSalary
			throw new IllegalArgumentException("Base salary must be >= 0.0");

		this.baseSalary = baseSalary;
	}

	// retorna o sal�rio-base continua
	public BigDecimal getBaseSalary() {
		return baseSalary;
	}

	// calcula os vencimentos; sobrescreve o m�todo earnings em CommissionEmployee
	@Override
	public BigDecimal getPaymentAmount() {
		return getBaseSalary().add(super.getPaymentAmount());
	}

	// retorna a representa��o String do objeto BasePlusCommissionEmployee
	@Override
	public String toString() {
		return String.format("%s %s; %s: $%,.2f", "Base-salaried", super.toString(), "Base salary", getBaseSalary());
	}

} // fim da classe BasePlusCommissionEmployee
