package br.com.deitel.capitulo10.folhaDePagamento;

import java.math.BigDecimal;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.9: PayrollSystemTest.java
 * Programa de teste da hierarquia Employee.
 */
public class PayrollSystemTest {
	
	public static void main(String[] args) {
		// cria objetos de subclasse
		SalariedEmployee salariedEmployee = new SalariedEmployee("John", "Smith", "111-11-1111", new BigDecimal(800.00));
		HourlyEmployee hourlyEmployee = new HourlyEmployee("Karen", "Price", "222-22-2222", 40, new BigDecimal(16.75));
		CommissionEmployee commissionEmployee = new CommissionEmployee("Sue", "Jones", "333-33-3333", new BigDecimal(10000), new BigDecimal(0.06));
		BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Bob", "Lewis", "444-44-4444", new BigDecimal(5000), new BigDecimal(.04), new BigDecimal(300));

		System.out.println("Employees processed individually:");
		System.out.printf("%n%s%n%s: $%.2f%n%n", salariedEmployee, "Earned", salariedEmployee.getPaymentAmount());
		System.out.printf("%s%n%s: $%,.2f%n%n", hourlyEmployee, "Earned", hourlyEmployee.getPaymentAmount());
		System.out.printf("%s%n%s: $%,.2f%n%n", commissionEmployee, "Earned", commissionEmployee.getPaymentAmount());
		System.out.printf("%s%n%s: $%,.2f%n%n", basePlusCommissionEmployee, "Earned", basePlusCommissionEmployee.getPaymentAmount());

		// cria um array Employee de quatro elementos
		Employee[] employees = new Employee[4];

		// inicializa o array com Employees
		employees[0] = salariedEmployee;
		employees[1] = hourlyEmployee;
		employees[2] = commissionEmployee;
		employees[3] = basePlusCommissionEmployee;

		System.out.printf("Employees processed polymorphically:%n%n");

		// processa genericamente cada elemento no employees
		for (Employee currentEmployee : employees) {
			System.out.println(currentEmployee); // invoca toString

			// determina se elemento � um BasePlusCommissionEmployee
			if (currentEmployee instanceof BasePlusCommissionEmployee) {
				// downcast da refer�ncia de Employee para
				// refer�ncia a BasePlusCommissionEmployee
				BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentEmployee;

				employee.setBaseSalary(new BigDecimal(1.10).multiply(employee.getBaseSalary()));

				System.out.printf("new base salary with 10%% increase is: $%,.2f%n", employee.getBaseSalary());
			} // fim do if

			System.out.printf("earned $%,.2f%n%n", currentEmployee.getPaymentAmount());
		} // for final

		// obt�m o nome do tipo de cada objeto no array employees
		for (int j = 0; j < employees.length; j++)
			System.out.printf("Employee %d is a %s%n", j, employees[j].getClass().getSimpleName());
	} // fim de main
	
} // fim da classe PayrollSystemTest