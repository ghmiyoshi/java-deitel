package br.com.deitel.capitulo10.folhaDePagamento;

import br.com.deitel.capitulo10.contasPagar.Payable;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.4: Employee.java 
 * Superclasse abstrata Employee.
 */
public abstract class Employee implements Payable {

	private final String firstName;
	private final String lastName;
	private final String socialSecurityNumber;

	// construtor
	public Employee(String firstName, String lastName, String socialSecurityNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
	}
	
	// retorna o nome
	public String getFirstName() {
		return firstName;
	}

	// retorna o sobrenome
	public String getLastName() {
		return lastName;
	}

	// retorna o n�mero do seguro social
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	// O m�todo abstract deve ser sobrescrito pelas subclasses concretas
	// public abstract BigDecimal earnings(); // nenhuma implementa��o aqui
	
	// retorna a representa��o de String do objeto Employee
	@Override
	public String toString() {
		return String.format("First name: %s %nLast name: %s %nSSN: %s", this.firstName, this.lastName, this.socialSecurityNumber);
	}
	
} // fim da classe abstrata Employee
