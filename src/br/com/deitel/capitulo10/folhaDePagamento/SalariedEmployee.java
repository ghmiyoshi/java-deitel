package br.com.deitel.capitulo10.folhaDePagamento;

import java.math.BigDecimal;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.5: SalariedEmployee.java
 * A classe concreta SalariedEmployee estende a classe abstrata Employee.
 */
public class SalariedEmployee extends Employee {

	private BigDecimal weeklySalary;

	// construtor
	public SalariedEmployee(String firstName, String lastName, String socialSecurityNumber, BigDecimal weeklySalary) {
		super(firstName, lastName, socialSecurityNumber);
		
		validWeeklySalary(weeklySalary);
		
		this.weeklySalary = weeklySalary;
	}
	
	// configura o sal�rio
	public void setWeeklySalary(BigDecimal weeklySalary) {
		validWeeklySalary(weeklySalary);
		this.weeklySalary = weeklySalary;
	}

	// retorna o sal�rio
	public BigDecimal getWeeklySalary() {
		return weeklySalary;
	}

	// calcula os rendimentos; sobrescreve o m�todo earnings em Employee
	// @Override
	// public BigDecimal earnings() {
	//	return getWeeklySalary();
	// }

	@Override
	public String toString() {
		return String.format("Salaried employee: R$ %.2f %n%s", getWeeklySalary(), super.toString());
	}

	private static void validWeeklySalary(BigDecimal weeklySalary) {
		if (weeklySalary.intValue() < 0.0)
			throw new IllegalArgumentException("Weekly salary must be >= 0.0");
	}

	// calcula vencimentos; implementa o m�todo Payable da interface que era abstrata na superclasse Employee
	@Override
	public BigDecimal getPaymentAmount() {
		return getWeeklySalary();
	}
	
	@Override
	public void testeDefault() {
		System.out.println("SalariedEmployee - Teste");
	}

}
