package br.com.deitel.capitulo10.folhaDePagamento;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.7: CommissionEmployee.java 
 * Classe CommissionEmployee estende Employee.
 */
public class CommissionEmployee extends Employee {

	private BigDecimal grossSales; // vendas brutas semanais
	private BigDecimal commissionRate; // porcentagem da comiss�o

	// construtor
	public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber, BigDecimal grossSales,
			BigDecimal commissionRate) {
		super(firstName, lastName, socialSecurityNumber);

		if (commissionRate.setScale(2, RoundingMode.HALF_EVEN).compareTo(BigDecimal.ZERO) <= 0.0 || commissionRate.intValue() >= 1.0) // valida
			throw new IllegalArgumentException("Commission rate must be > 0.0 and < 1.0");

		if (grossSales.intValue() < 0.0) // valida
			throw new IllegalArgumentException("Gross sales must be >= 0.0");

		this.grossSales = grossSales;
		this.commissionRate = commissionRate;
	}

	// configura a quantidade de vendas brutas
	public void setGrossSales(BigDecimal grossSales) {
		if (grossSales.intValue() < 0.0) // valida
			throw new IllegalArgumentException("Gross sales must be >= 0.0");

		this.grossSales = grossSales;
	}

	// retorna a quantidade de vendas brutas
	public BigDecimal getGrossSales() {
		return grossSales;
	}

	// configura a taxa de comiss�o
	public void setCommissionRate(BigDecimal commissionRate) {
		if (commissionRate.intValue() <= 0.0 || commissionRate.intValue() >= 1.0) // valida
			throw new IllegalArgumentException("Commission rate must be > 0.0 and < 1.0");

		this.commissionRate = commissionRate;
	}

	// retorna a taxa de comiss�o
	public BigDecimal getCommissionRate() {
		return commissionRate;
	}

	// calcula os rendimentos; sobrescreve o m�todo earnings em Employee
	@Override
	public BigDecimal getPaymentAmount() {
		return getCommissionRate().multiply(getGrossSales());
	}

	// retorna a representa��o String do objeto CommissionEmployee
	@Override
	public String toString() {
		return String.format("%s: %s%n%s: $%,.2f; %s: %.2f", "Commission employee", super.toString(), "Gross sales",
				getGrossSales(), "Commission rate", getCommissionRate());
	}
	
} // fim da classe CommissionEmployee
