package br.com.deitel.capitulo10.folhaDePagamento;

import java.math.BigDecimal;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.6: HourlyEmployee.java 
 * Classe HourlyEmployee estende Employee.
 */
public class HourlyEmployee extends Employee {

	private int hours; // horas trabalhadas durante a semana
	private BigDecimal wage; // salário por hora

	// construtor
	public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, int hours, BigDecimal wage) {
		super(firstName, lastName, socialSecurityNumber);
		validateParams(wage, hours);
		this.hours = hours;
		this.wage = wage;
	}

	// retorna as horas trabalhadas
	public int getHours() {
		return hours;
	}

	// configura as horas trabalhadas
	public void setHours(int hours) {
		this.hours = hours;
	}

	// retorna a remuneração
	public BigDecimal getWage() {
		return wage;
	}

	// configura a remuneração
	public void setWage(BigDecimal wage) {
		this.wage = wage;
	}

	// calcula os rendimentos; sobrescreve o método earnings em Employee
	@Override
	public BigDecimal getPaymentAmount() {
		if (getHours() <= 40) { // nenhuma hora extra
			return getWage().multiply(new BigDecimal(getHours()));
		} else {
			return getWage().multiply(new BigDecimal(40))
					.add((new BigDecimal(getHours() - 40).multiply(getWage().multiply(new BigDecimal(1.5)))));
		}
	}

	private static void validateParams(BigDecimal wage, int hours) {
		if (wage.intValue() < 0.0) // valida remuneração
			throw new IllegalArgumentException("Hourly wage must be >= 0.0");

		if ((hours < 0.0) || (hours > 168.0)) // valida horas
			throw new IllegalArgumentException("Hours worked must be >= 0.0 and <= 168.0");
	}

	// retorna a representação de String do objeto HourlyEmployee
	@Override
	public String toString() {
		return String.format("Hourly employee: %s%n%s: R$%.2f %s: %d", super.toString(), "Hourly wage", getWage(),
				"Hours worked", getHours());
	}
	
} // fim da classe HourlyEmployee