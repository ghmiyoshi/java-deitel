package br.com.deitel.capitulo10.contasPagar;

import java.math.BigDecimal;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.12: Invoice.java
 * Classe Invoice que implementa Payable.
 */
public class Invoice implements Payable {

	private final String partNumber;
	private final String partDescription;
	private int quantity;
	private BigDecimal pricePerItem;

	// construtor
	public Invoice(String partNumber, String partDescription, int quantity, BigDecimal pricePerItem) {
		if (quantity < 0) // valida quantidade
			throw new IllegalArgumentException("Quantity must be >= 0");

		if (pricePerItem.compareTo(BigDecimal.ZERO) < 0.0) // valida pricePerItem
			throw new IllegalArgumentException("Price per item must be >= 0");

		this.quantity = quantity;
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.pricePerItem = pricePerItem;
	} // fim do construtor

	// obt�m o n�mero da pe�a
	public String getPartNumber() {
		return partNumber; // deve validar
	}

	// obt�m a descri��o
	public String getPartDescription() {
		return partDescription;
	}

	// configura a quantidade
	public void setQuantity(int quantity) {
		if (quantity < 0) // valida quantidade
			throw new IllegalArgumentException("Quantity must be >= 0");

		this.quantity = quantity;
	}

	// obt�m quantidade
	public int getQuantity() {
		return quantity;
	}

	// configura pre�o por item
	public void setPricePerItem(BigDecimal pricePerItem) {
		if (pricePerItem.compareTo(BigDecimal.ZERO) < 0.0) // valida pricePerItem
			throw new IllegalArgumentException("Price per item must be >= 0");

		this.pricePerItem = pricePerItem;
	}

	// obt�m pre�o por item
	public BigDecimal getPricePerItem() {
		return pricePerItem;
	}

	// retorno da representa��o de String do objeto Invoice
	@Override
	public String toString() {
		return String.format("%s: %n%s: %s (%s) %n%s: %d %n%s: $%,.2f", 
				"invoice", 
				"part number", 
				getPartNumber(),
				getPartDescription(), 
				"quantity", 
				getQuantity(), 
				"price per item", 
				getPricePerItem());
	}

	// m�todo requerido para executar o contrato com a interface Payable
	@Override
	public BigDecimal getPaymentAmount() {
		return getPricePerItem().multiply(new BigDecimal(getQuantity())); // calcula custo total
	}
	
} // fim da classe Invoice
