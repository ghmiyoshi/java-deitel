package br.com.deitel.capitulo10.contasPagar;

import java.math.BigDecimal;

/**
 * @author Gabriel Hideki
 * 
 * Figura 10.11: Payable.java
 * Declaração da interface Payable.
 */
public interface Payable {
	
	BigDecimal getPaymentAmount(); // calcula pagamento; nenhuma implementação
	
	default void testeDefault() {
		System.out.println("Payable - Teste");
	}

}
