package br.com.deitel.capitulo10.contasPagar;

import java.math.BigDecimal;

import br.com.deitel.capitulo10.folhaDePagamento.SalariedEmployee;


/**
 * @author Gabriel Hideki
 * 
 * Figura 10.15: PayableInterfaceTest.java
 * Programa de teste da interface Payable que processa Invoices e Employees polimorficamente.
 */
public class PayableInterfaceTest {

	public static void main(String[] args) {
		// cria array Payable de quatro elementos
		Payable[] payableObjects = new Payable[4];

		// preenche o array com objetos que implementam Payable
		payableObjects[0] = new Invoice("01234", "seat", 2, new BigDecimal(375.00));
		payableObjects[1] = new Invoice("56789", "tire", 4, new BigDecimal(79.95));
		payableObjects[2] = new SalariedEmployee("John", "Smith", "111-11-1111", new BigDecimal(800.00));
		payableObjects[3] = new SalariedEmployee("Lisa", "Barnes", "888-88-8888", new BigDecimal(1200.00));

		payableObjects[0].testeDefault();
		payableObjects[3].testeDefault();
		
		System.out.println("Invoices and Employees processed polymorphically:");

		// processa genericamente cada elemento no array payableObjects
		for (Payable currentPayable : payableObjects) {
			// gera sa�da de currentPayable e sua quantia de pagamento apropriado
			System.out.printf("%n%s %n%s: R$%.2f%n", currentPayable.toString(), // poderia invocar implicitamente
					"payment due", currentPayable.getPaymentAmount());
		}
	} // fim de main

} // fim da classe PayableInterfaceTest