package br.com.deitel.capitulo2;

/**
 * @author Gabriel Hideki
 * 
 * Figura 2.3: Welcome2.java
 * Programa de impress�o de multiplas linhas de texto.
 * 
 */
public class Welcome2 {
	
	// m�todo main inicia a execu��o do aplicativo Java
	public static void main(String[] args) {
		System.out.print("Welcome to "); 
		System.out.println("Java Programming!");
	} // fim do m�todo main
	
} // fim da classe Welcome1
