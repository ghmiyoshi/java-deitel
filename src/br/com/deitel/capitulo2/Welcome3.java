package br.com.deitel.capitulo2;

/**
 * @author Gabriel Hideki
 * 
 * Figura 2.4: Welcome3.java
 * Imprimindo m�ltiplas linhas de texto com uma �nica instru��o.
 */
public class Welcome3 {
	
	// m�todo main inicia a execu��o do aplicativo Java
	public static void main(String[] args) {
		System.out.print("Welcome\nto\nJava\nProgramming!\n");
		System.out.println("\"entre aspas\"\n");
		System.out.println("\\"); // // - imprimi \
		System.out.println("\""); // \" - imprimi "
		
		System.out.printf("%s%n%s%n", "Welcome to", "Java Programming");
	} // fim do m�todo main
	
} // fim da classe Welcome1
