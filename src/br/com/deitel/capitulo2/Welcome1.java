package br.com.deitel.capitulo2;

/**
 * @author Gabriel Hideki
 * 
 * Figura 2.1: Welcome1.java
 * Programa de impress�o de texto.
 */
public class Welcome1 {
	
	// m�todo main inicia a execu��o do aplicativo Java
	public static void main(String[] args) {
		System.out.println("Welcome to Java Programming!");
	} // fim do m�todo main
	
} // fim da classe Welcome1
